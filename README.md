# Anonimo

## What is this project about?

Anonimo is an authentication-less messaging web application centered around anonimity.

## Installing, running the project & contributing

In order to run/install the whole stack, follow the instructions in the following repositories in the order that they are listed.

1. [server](https://gitlab.com/Anonimo/server)
2. [webui](https://gitlab.com/Anonimo/webui)
